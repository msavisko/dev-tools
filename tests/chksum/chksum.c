// Incrementally update checksum when modifying a 16-bit value.
static inline void checksum_update_incremental_16(uint16_t* checksum_cell, uint16_t* value_cell, uint16_t new_value) {
    uint32 sum;

    sum = ~(*checksum_cell) & 0xffff;
    sum += (~(*value_cell) & 0xffff) + new_value;
    sum = (sum >> 16) + (sum & 0xffff);
    sum += (sum >> 16);

    *checksum_cell = ~sum & 0xffff;
    *value_cell = new_value;
}

// Incrementally update checksum when modifying a 32-bit value.
static inline void checksum_update_incremental_32(uint16_t* checksum_cell, uint32_t* value_cell, uint32_t new_value) {
    uint32_t sum;

    uint32_t old_value = ~(*value_cell);

    sum = ~(*checksum_cell) & 0xffff;
    sum += (old_value >> 16) + (old_value & 0xffff);
    sum += (new_value >> 16) + (new_value & 0xffff);
    sum = (sum >> 16) + (sum & 0xffff);
    sum += (sum >> 16);

    *checksum_cell = ~sum & 0xffff;
    *value_cell = new_value;
}
