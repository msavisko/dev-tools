#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>

#define NUM_TEST_ITTERATIONS	100000000UL

#define likely(x)    __builtin_expect(!!(x), 1)
#define unlikely(x)  __builtin_expect(!!(x), 0)

typedef union {
	__int128 m128;
	struct {
		uint64_t  val;
		uint64_t  ts;
	};
} dwcas_t __attribute__((aligned(16)));

static inline __attribute__ ((always_inline)) uint64_t
rdtsc(void)
{
        union {
                uint64_t tsc_64;
                struct {
                        uint32_t lo_32;
                        uint32_t hi_32;
                };
        } tsc;

        asm volatile("rdtsc" :
                     "=a" (tsc.lo_32),
                     "=d" (tsc.hi_32));
        return tsc.tsc_64;
}

extern void cmpxchg16b_sync_version(dwcas_t *ptr, uint64_t desired);

void __attribute__ ((noinline)) cmpxchg16b_sync_version(dwcas_t *ptr, uint64_t desired)
{
	dwcas_t oldval, newval;

	newval.val = desired;

restart:
	oldval.m128 = ptr->m128;
	newval.ts = oldval.ts + 1UL;
	if (unlikely(!__sync_bool_compare_and_swap(&ptr->m128, oldval.m128, newval.m128))) {
#if defined(__x86_64__) || defined(__i386__)
		__asm__ __volatile__ ("pause" ::: "memory");
#endif
		goto restart;
	}
}

extern void cmpxchg16b_sync_version2(dwcas_t *ptr, uint64_t desired);

void __attribute__ ((noinline)) cmpxchg16b_sync_version2(dwcas_t *ptr, uint64_t desired)
{
	dwcas_t oldval, newval, tmpval;

	oldval.m128 = ptr->m128;
	newval.val = desired;

restart:
	newval.ts = oldval.ts + 1UL;
	tmpval.m128 = __sync_val_compare_and_swap(&ptr->m128, oldval.m128, newval.m128);
	if (unlikely(tmpval.m128 != oldval.m128)) {
#if defined(__x86_64__) || defined(__i386__)
		__asm__ __volatile__ ("pause" ::: "memory");
#endif
		oldval.m128 = tmpval.m128;
		goto restart;
	}
}

extern void cmpxchg16b_atomic_version(dwcas_t *ptr, uint64_t desired);

void __attribute__ ((noinline)) cmpxchg16b_atomic_version(dwcas_t *ptr, uint64_t desired)
{
	dwcas_t oldval, newval;

	oldval.m128 = ptr->m128;
	newval.val = desired;

restart:
	newval.ts = oldval.ts + 1UL;
	if (unlikely(!__atomic_compare_exchange_n(&ptr->m128, &oldval.m128,
						newval.m128, 1,
						__ATOMIC_ACQ_REL,
						__ATOMIC_ACQUIRE))) {
#if defined(__x86_64__) || defined(__i386__)
		__asm__ __volatile__ ("pause" ::: "memory");
#endif
		goto restart;
	}
}

typedef void (*cmpxchg16b_func_t)(dwcas_t *ptr, uint64_t desired);

uint64_t test_cmpxchg16b_version(cmpxchg16b_func_t f, uint64_t cycles)
{
	dwcas_t val = { .m128 = 0 };
	uint64_t i, start_ts, end_ts;

	__asm__ __volatile__ ("" ::: "memory");
	start_ts = rdtsc();

	for (i = 0; i < cycles; i++)
		f(&val, i);

	end_ts = rdtsc();
	return end_ts - start_ts;
}

extern bool lock(int *l);

bool lock(int *l)
{
	int tmp = 0;
	return __atomic_compare_exchange_n(l, &tmp, 1,
				false, __ATOMIC_ACQ_REL, __ATOMIC_ACQUIRE);
}

extern void unlock(int *l);

void unlock(int *l)
{
	//__atomic_store_n(l, 0, __ATOMIC_SEQ_CST);
	__atomic_store_n(l, 0, __ATOMIC_RELEASE);
}

int main()
{
	uint64_t t1, t2, t3;

	t1 = test_cmpxchg16b_version(&cmpxchg16b_sync_version, NUM_TEST_ITTERATIONS);
	t2 = test_cmpxchg16b_version(&cmpxchg16b_sync_version2, NUM_TEST_ITTERATIONS);
	t3 = test_cmpxchg16b_version(&cmpxchg16b_atomic_version, NUM_TEST_ITTERATIONS);

	printf("cmpxchg16b_sync_version   took %lu cycles, %lu cycles per 1 iteration\n",
		t1, t1 / NUM_TEST_ITTERATIONS);
	printf("cmpxchg16b_sync_version2  took %lu cycles, %lu cycles per 1 iteration\n",
		t2, t2 / NUM_TEST_ITTERATIONS);
	printf("cmpxchg16b_atomic_version took %lu cycles, %lu cycles per 1 iteration\n",
		t3, t3 / NUM_TEST_ITTERATIONS);

	return 0;
}


