#include <stdbool.h>
#include <stddef.h>


typedef struct {
	/* Put your app data here */
} service_graph_t;

typedef struct {
	service_graph_t *service_graph;
	service_graph_t *new_service_graph;
	void            *update_args;
	unsigned int     service_graph_update_lcore_id;
} ctx_handle_t;

typedef void *ctx_handle;

#define __ALWAYS_INLINE__  __attribute((always_inline))

#ifndef likely
#define likely(x)   __builtin_expect(!!(x),1)
#endif
#ifndef unlikely
#define unlikely(x) __builtin_expect(!!(x),0)
#endif

#define INVALID_LCORE_ID ((unsigned int) 0xFFFFFFFFu)

#if defined(__x86_64__) || defined(__x86_64)
#define COMPILER_MEMORY_BARRIER()  __asm__ __volatile__ ("" : : : "memory")

#define MFENCE()  __asm__ __volatile__ ("mfence" : : : "memory")

#define SFENCE()  __asm__ __volatile__ ("sfence" : : : "memory")

#define LFENCE()  __asm__ __volatile__ ("lfence" : : : "memory")
#else
#error "build currently supported only for __x86_64__ architecture"
#endif


extern bool begin_update_graph(ctx_handle ctx_handle);
extern bool commit_update_graph(ctx_handle ctx_handle, service_graph_t *new_service_graph);


extern service_graph_t* take_service_graph_slowpath(ctx_handle ctx_handle, unsigned int lcore_id);
extern void             put_service_graph_slowpath(ctx_handle ctx_handle, service_graph_t *used_graph, unsigned int lcore_id);


static inline __ALWAYS_INLINE__ service_graph_t*
take_service_graph(ctx_handle ctx_handle, unsigned int lcore_id)
{
    if (likely( !((ctx_handle_t*)ctx_handle)->new_service_graph ))
    {
        return ((ctx_handle_t*)ctx_handle)->service_graph;
    }
    return take_service_graph_slowpath(ctx_handle, lcore_id);
}

static inline __ALWAYS_INLINE__ void
put_service_graph(ctx_handle ctx_handle, service_graph_t *used_graph, unsigned int lcore_id)
{
    if (likely( ((ctx_handle_t*)ctx_handle)->service_graph == used_graph ))
    {
        return;
    }
    put_service_graph_slowpath(ctx_handle, used_graph, lcore_id);
}

static inline __ALWAYS_INLINE__ bool
update_in_progress(ctx_handle ctx_handle)
{
    return ((ctx_handle_t*)ctx_handle)->new_service_graph ? true : false;
}

static inline __ALWAYS_INLINE__ void
clear_update_in_progress(ctx_handle ctx_handle)
{
    ((ctx_handle_t*)ctx_handle)->new_service_graph = NULL;
    ((ctx_handle_t*)ctx_handle)->update_args = NULL;
}
