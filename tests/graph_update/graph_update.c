#include <stdlib.h>

#include "graph_update.h"


typedef struct {
	// Put here all you need for graph update process
} graph_update_completion_args_t;


typedef void (*mgmt_task_func_arg3_t)(void *, void *, void *);

// Send request to management core to execution specific function with three arguments
extern int management_enqueue_task_arg3(mgmt_task_func_arg3_t func, void *arg1, void *arg2, void *arg3);


static void complete_update_graph_task(void *arg1, void *arg2, void *arg3);


static bool service_graph_update_start(ctx_handle ctx_handle, service_graph_t *new_graph, graph_update_completion_args_t *args)
{
    void *expected_value, *read_value;
    ctx_handle_t *ctx = (ctx_handle_t*) ctx_handle;

    expected_value = NULL;

    MFENCE();

    read_value = __sync_val_compare_and_swap(&(ctx->update_args), expected_value, (void*)args);

    if (unlikely(read_value != expected_value))
        return false;

    SFENCE();

    ctx->new_service_graph = new_graph;

    SFENCE();

    return true;
}

bool begin_update_graph(ctx_handle ctx_handle)
{
    if (unlikely(!ctx_handle))
    {
        return false;
    }

    if (likely(update_in_progress(ctx_handle) == false))
        return true;

    return false;
}

bool commit_update_graph(ctx_handle ctx_handle, service_graph_t *new_service_graph)
{
    graph_update_completion_args_t *completion_arguments = NULL;

	if (!ctx_handle || !new_service_graph)
    {
        return false;
    }

    if (((ctx_handle_t *)ctx_handle)->service_graph == new_service_graph)
    {
        return false;
    }

	completion_arguments = malloc(sizeof(*completion_arguments));
	if (!completion_arguments)
		return false;
	// Fill in completion_arguments

    if (!service_graph_update_start(ctx_handle, new_service_graph, completion_arguments))
    {
        //LOG_ERROR_S("service_graph_update_start failed");
		free(completion_arguments);
		return false;
    }

    return true;
}


service_graph_t* take_service_graph_slowpath(ctx_handle ctx_handle, unsigned int lcore_id)
{
    ctx_handle_t *ctx = (ctx_handle_t*) ctx_handle;
    service_graph_t *service_graph = ctx->service_graph;
    service_graph_t *new_service_graph = ctx->new_service_graph;
    service_graph_t *read_value;

    if (service_graph == new_service_graph)
    {
        //LOG_INFO("LCORE %u in %s: service_graph == new_service_graph -> %p", lcore_id, __FUNCTION__, new_service_graph);
        return service_graph;
    }

    //LOG_INFO("LCORE %u in %s: service_graph=%p, new_service_graph=%p", lcore_id, __FUNCTION__, (void*)service_graph, (void*)new_service_graph);

    MFENCE();

    new_service_graph = ctx->new_service_graph;
    if (!new_service_graph)
    {
        //LOG_INFO("LCORE %u in %s: new_service_graph== NULL - update already completed by another lcore",  lcore_id, __FUNCTION__);
        return ctx->service_graph;
    }

    /*
     * This code block below is executed atomically:
     *  read_value = ctx->service_graph;
     *  if (ctx->service_graph == service_graph)
     *     ctx->service_graph = new_service_graph;
     */
    read_value = __sync_val_compare_and_swap(&(ctx->service_graph), service_graph, new_service_graph);

    SFENCE();

    /*
     * The ctx->service_graph will be initialized with the new_service_graph only on a SINGLE thread.
     * For the single thread that made the change we will return the OLD service graph.
     * This will ensure that the (single) thread that made the change will complete the update process by sending the clean task to the management thread.
     *
     * The thread that will enter this 'if clause' will be the only service bus thread that succeeded in   __sync_val_compare_and_swap above,
     * and that thread will be the only thread that will continue to sending task to management thread in put_service_graph_completion().
     */
    if (likely(read_value == service_graph))
    {
        //LOG_INFO("LCORE %u in %s: successfully set new_service_graph=%p as service_graph, returning old service_graph=%p", lcore_id, __FUNCTION__, (void*)new_service_graph, (void*)service_graph);
        ctx->service_graph_update_lcore_id = lcore_id;
        SFENCE();
        return service_graph;
    }

    //LOG_INFO("LCORE %u in %s: failed to set new_service_graph=%p as service_graph, returning new_service_graph=%p", lcore_id, __FUNCTION__, (void*)new_service_graph, (void*)new_service_graph);
    return new_service_graph;
}

void put_service_graph_slowpath(ctx_handle ctx_handle, service_graph_t *used_graph, unsigned int lcore_id)
{
    int result;
    ctx_handle_t *ctx;
    //uint64_t start_ts, end_ts;
    void *update_args;

    //LOG_INFO("LCORE %u: %s started: used_graph=%p", lcore_id, __FUNCTION__, (void*)used_graph);

    ctx = (ctx_handle_t*) ctx_handle;
    if (ctx->service_graph_update_lcore_id != lcore_id)
    {
        //LOG_INFO("LCORE %u: not owner - should do nothing", lcore_id);
        return;
    }

    //COMPILER_MEMORY_BARRIER();
    //start_ts = get_timer_cycles();
    //COMPILER_MEMORY_BARRIER();

    if (unlikely(ctx->new_service_graph == NULL))
    {
        //LOG_ERROR("LCORE %u: dynamic graph update BUG!!! ctx->new_service_graph==NULL", lcore_id);
        return;
    }

    if (unlikely(ctx->service_graph == used_graph))
    {
        //LOG_ERROR("LCORE %u: dynamic graph update BUG!!! ctx->service_graph==used_graph==%p", lcore_id, (void*)used_graph);
        return;
    }

    update_args = ctx->update_args;
    ctx->update_args = NULL;
    SFENCE();
    ctx->new_service_graph = NULL;
    SFENCE();

    ctx->service_graph_update_lcore_id = INVALID_LCORE_ID;

    result = management_enqueue_task_arg3(&complete_update_graph_task, ctx, used_graph, update_args);
    if (unlikely(result != 0))
    {
        //LOG_ERROR("LCORE %u: failed to enqueue management task", lcore_id);
        return;
    }

    //COMPILER_MEMORY_BARRIER();
    //end_ts = get_timer_cycles();
    //COMPILER_MEMORY_BARRIER();

    //LOG_INFO("LCORE %u: %s ended: took %lu cycles", lcore_id, __FUNCTION__, (end_ts - start_ts));
}

static void wait_before_old_graph_destruction(service_graph_t *old_graph)
{
	// TODO: find ways to find out when nobody's touching old_graph
    (void)old_graph;
}

static void complete_update_graph_task(void *arg1, void *arg2, void *arg3)
{
    ctx_handle_t *curr_ctx;
	ctx_handle curr_ctx_handle = (ctx_handle) arg1;
    service_graph_t *old_graph = (service_graph_t*)arg2;
    graph_update_completion_args_t *update_args = (graph_update_completion_args_t*) arg3;

    //LOG_INFO_S("started");

    if (unlikely(!curr_ctx_handle))
    {
        //LOG_ERROR_S("no current CTX");
        return;
    }
    if (unlikely(!old_graph))
    {
        //LOG_ERROR_S("no old_graph argument");
        return;
    }

    curr_ctx = (ctx_handle_t*)curr_ctx_handle;
    (void)curr_ctx;

    if (update_args)
    {
		// do what you need with update args

		free(update_args);
    }

    wait_before_old_graph_destruction(old_graph);

    //LOG_INFO_S("finished");
}
