#!/bin/bash -xe

modprobe kvm
modprobe kvm_intel

systemctl start  libvirtd.service
systemctl status libvirtd.service
