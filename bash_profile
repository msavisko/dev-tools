# .bash_profile

export BASH_PROFILE_SOURCED=1

# Get the aliases and functions
if [ -f ~/.bashrc ]; then
	. ~/.bashrc
fi

pathmunge () {
    case ":${PATH}:" in
        *:"$1":*)
            ;;
        *)
            if [ "$2" = "after" ] ; then
                PATH=$PATH:$1
            else
                PATH=$1:$PATH
            fi
    esac
}

# User specific environment and startup programs

pathmunge $HOME/bin after

export PATH

# Setup LD_LIBRARY_PATH variable
if [ -f $HOME/.ld_library_path ]; then
	. $HOME/.ld_library_path
fi

