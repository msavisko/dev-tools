# .bashrc

# User specific aliases and functions

alias rm='rm -i'
alias cp='cp -i'
alias mv='mv -i'

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

if [ -f $HOME/.aliases ]; then 
	. $HOME/.aliases
fi

# Source .bash_profile if needed
if [ -f $HOME/.bash_profile -a "$BASH_PROFILE_SOURCED" == "" ]; then
	. $HOME/.bash_profile
fi

# Setup LD_LIBRARY_PATH variable
if [ -f $HOME/.ld_library_path ]; then
	. $HOME/.ld_library_path
fi

TZ='Asia/Jerusalem'; export TZ

if [ "$PS1" != "" ]; then
        export PS1='[\u@\h \W]\$'
fi

