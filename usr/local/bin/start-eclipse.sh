if [ "$ECLIPSE_HOME" == "" ]; then
	ECLIPSE_HOME=/opt/eclipse
fi

if [ ! -x ${ECLIPSE_HOME}/eclipse ]; then
	echo "Cannot find Eclipse executable: ${ECLIPSE_HOME}/eclipse"
	exit 1
fi

#export SWT_GTK3=0

nohup ${ECLIPSE_HOME}/eclipse > /dev/null 2>& 1 &

exit 0

